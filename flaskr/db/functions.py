import os
from mysql import connector

def get_db_connection():
    # conn = connector.connect(
    #     host=os.environ['DB_HOST'],
    #     database=os.environ['DB_DATABASE'],
    #     user=os.environ['DB_USERNAME'],
    #     password=os.environ['DB_PASSWORD'],
    #     port=os.environ['DB_PORT']
    # )
    # return conn

    conn = connector.connect(
        host='127.0.0.1',
        database='theaterDb',
        user='root',
    )
    return conn
