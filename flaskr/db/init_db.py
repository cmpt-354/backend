#import os
from mysql import connector

conn = connector.connect(
    host='db-mysql-sfo2-67879-do-user-11929187-0.b.db.ondigitalocean.com',
    database='theaterDb',
    user='doadmin',
    password='AVNS_Da0e7Z0L5Nyxgp1v0Qo',
    port=25060
)

# Open a cursor to perform database operations
cur = conn.cursor()

# Execute a command: this creates a new table

#cur.execute('DROP TABLE IF EXISTS Department;')
#cur.execute(
#    'CREATE TABLE Department ('
#        'ID int AUTO_INCREMENT,'
#        'Name varchar(255) NOT NULL,'
#        'PRIMARY KEY (ID)'
#    ');'
#)
cur.execute('SET @ORIG_SQL_REQUIRE_PRIMARY_KEY = @@SQL_REQUIRE_PRIMARY_KEY;')
cur.execute('SET SQL_REQUIRE_PRIMARY_KEY = 0;')
cur.execute('DROP TABLE IF EXISTS Venue;')
cur.execute(
    'CREATE TABLE Venue ('
        'ID int AUTO_INCREMENT,'
        'Name varchar(255),'
        'Address varchar(255),'
        'PRIMARY KEY(ID)'
    ');'
)
cur.execute('DROP TABLE IF EXISTS Manager;')
cur.execute('DROP TABLE IF EXISTS ShowEquipment;')
cur.execute('DROP TABLE IF EXISTS TechnicianShow;')
cur.execute('DROP TABLE IF EXISTS TheaterShow CASCADE;')
cur.execute('DROP TABLE IF EXISTS Technician CASCADE;')
cur.execute('DROP TABLE IF EXISTS User CASCADE;')
cur.execute(
   'CREATE TABLE User ('
       'ID int AUTO_INCREMENT,'
       'FirstName varchar(255),'
       'LastName varchar(255),'
       'Email varchar(255),'
       'Active boolean,'
       'StaffNumber int NOT NULL,'
       'PRIMARY KEY(ID),'
       'UNIQUE(StaffNumber)'
   ');'
)

cur.execute('DROP TABLE IF EXISTS UserContact;')
cur.execute(
    'CREATE TABLE UserContact ('
        'UserID int NOT NULL REFERENCES User(ID) ON DELETE CASCADE,'
        'Phone varchar(15) NOT NULL,'
        'Address varchar(255) NOT NULL,'
        'UNIQUE(UserID, Phone, Address)'
    ');'
)

cur.execute(
    'CREATE TABLE Manager ('
        'UserID int,'
        'LastName varchar(255),'
        'FirstName varchar(255),'
        'Email varchar(255),'
        'StaffNumber int,'
        'PhoneNumber varchar(255),'
        'VenueID int REFERENCES Venue(ID) ON DELETE SET NULL,'
        'PRIMARY KEY(UserID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Technician;')
cur.execute(
        'CREATE TABLE Technician ('
            'UserID int,'
            'DepartmentID int REFERENCES Department(ID) ON DELETE SET NULL,'
            'PRIMARY KEY(UserID)'
        ');'
)

cur.execute('DROP TABLE IF EXISTS TheaterShow;')
cur.execute(
        'CREATE TABLE TheaterShow ('
            'Name varchar(255) NOT NULL,'
            'Season varchar(255) NOT NULL,'
            'UNIQUE (Season, Name)'
        ');'
)

cur.execute(
    'CREATE TABLE TechnicianShow ('
        'TechnicianID int NOT NULL,'
        'ShowSeason int NOT NULL,'
        'ShowName varchar(255) NOT NULL,'
#        'FOREIGN KEY(TechnicianID) REFERENCES Technician(UserID) ON DELETE CASCADE,'
#        'FOREIGN KEY(ShowSeason, ShowName) REFERENCES TheaterShow(Name, Season) ON DELETE CASCADE,'
        'UNIQUE(TechnicianID, ShowSeason, ShowName)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS ShowType;')
cur.execute(
        'CREATE TABLE ShowType ('
            'Name varchar(255),'
            'Type varchar(255),'
            'PRIMARY KEY(Name, Type)'
        ');'
)

cur.execute('DROP TABLE IF EXISTS Equipment;')
cur.execute(
    'CREATE TABLE Equipment ('
        'ID int AUTO_INCREMENT,'
        'VenueID int REFERENCES Venue(ID) ON DELETE SET NULL,'
        'DepartmentID int,'
        'Name varchar(255),'
        'PRIMARY KEY(ID),'
        'FOREIGN KEY(DepartmentID) REFERENCES Department(ID) ON DELETE SET NULL'
    ');'
)

cur.execute('DROP TABLE IF EXISTS ShowEquipment;')
cur.execute(
    'CREATE TABLE ShowEquipment ('
        'ShowSeason int NOT NULL,'
        'ShowName varchar(255) NOT NULL,'
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'StartDate datetime NOT NULL,'
        'EndDate datetime NOT NULL,'
#        'FOREIGN KEY(ShowSeason, ShowName) REFERENCES TheaterShow(Season, Name) ON DELETE CASCADE,'
        'UNIQUE(EquipmentID, ShowName, ShowSeason, StartDate, EndDate)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Computer;')
cur.execute(
    'CREATE TABLE Computer ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'Year int,'
        'Ram varchar(255),'
        'OS varchar(255),'
        'Model varchar(255),'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Cable;')
cur.execute(
    'CREATE TABLE Cable ('
        'DepartmentID int,'
        'Name varchar(255),'
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'Length int,'
        'VenueID int,'
        'Connectors varchar(255),'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Mic;')
cur.execute(
    'CREATE TABLE Mic ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'Length double,'
        'Connectors varchar(255),'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Speaker;')
cur.execute(
    'CREATE TABLE Speaker ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'PRIMARY KEY(EquipmentID)'
        ');'
)

cur.execute('DROP TABLE IF EXISTS SoundDevice;')
cur.execute(
    'CREATE TABLE SoundDevice ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Light;')
cur.execute(
    'CREATE TABLE Light ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'Bulb varchar(255),'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Gel;')
cur.execute(
    'CREATE TABLE Gel ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'Color varchar(255),'
        'Size int,'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS LightingDevice;')
cur.execute(
    'CREATE TABLE LightingDevice ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Projector;')
cur.execute(
    'CREATE TABLE Projector ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'Bulb varchar(255),'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Monitor;')
cur.execute(
    'CREATE TABLE Monitor ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'Connectors varchar(255),'
        'PRIMARY KEY (EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS Camera;')
cur.execute(
    'CREATE TABLE Camera ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'Connectors varchar(255),'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS VideoDevice;')
cur.execute(
    'CREATE TABLE VideoDevice ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'PRIMARY KEY(EquipmentID)'
    ');'
)

cur.execute('DROP TABLE IF EXISTS DefaultDevice;')
cur.execute(
    'CREATE TABLE DefaultDevice ('
        'EquipmentID int REFERENCES Equipment(ID) ON DELETE CASCADE,'
        'PRIMARY KEY(EquipmentID)'
    ');'
)



conn.commit()

cur.close()
conn.close()