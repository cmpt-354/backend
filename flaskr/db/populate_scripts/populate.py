## Adapted from projectpro.io 

import os
from mysql import connector
from mysql.connector import Error
import pandas as pd
from requests import head


try:
    conn = connector.connect(
        host='127.0.0.1',
        database='theaterDb',
        user='root',
    )
#    conn = connector.connect(
#        host=os.environ['DB_HOST'],
#        database=os.environ['DB_DATABASE'],
#        user=os.environ['DB_USERNAME'],
#        password=os.environ['DB_PASSWORD'],
#        port=os.environ['DB_PORT']
#    )
    if conn.is_connected():

        cursor = conn.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)

        ## CABLE ##
        print("cable")
        path_to_csv = './cable.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Cable VALUES (%s,%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

        # ## CAMERA ##
        print("camera")
        path_to_csv = './camera.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')
        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Camera VALUES (%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

        ## COMPUTER ##
        print("computer")
        path_to_csv = './computer.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Computer VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## DEFAULT DEVICE ##
        print("defautl dev")
        path_to_csv = './default_device.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.DefaultDevice VALUES (%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## DEPARTMENT ##
        print("department")
        path_to_csv = './department.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Department VALUES (%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## GEL ##
        print("gel")
        path_to_csv = './gel.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Gel VALUES (%s,%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## LIGHT ##
        print("light")
        path_to_csv = './light.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Light VALUES (%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## LIGHTING DEVICE ##
        print("ligtingdevice")
        path_to_csv = './lighting_device.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.LightingDevice VALUES (%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## MANAGER ##
        print("manager")
        path_to_csv = './manager.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Manager VALUES (%s,%s,%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## MIC ##
        print("Mic")
        path_to_csv = './mic.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Mic VALUES (%s,%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## MONITOR ##
        print("monitor")
        path_to_csv = './monitor.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Monitor VALUES (%s,%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## PROJECTOR ##
        print("projector")
        path_to_csv = './projector.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Projector VALUES (%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## SHOW EQUIPMENT ##
        print("show equiptme")
        path_to_csv = './show_equipment.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.ShowEquipment VALUES (%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## SHOW TYPE ##
        print("showtype")
        path_to_csv = './show_type.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.ShowType VALUES (%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## SOUND DEVICE ##
        print("sound device")
        path_to_csv = './sound_device.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.SoundDevice VALUES (%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## SPEAKER ##
        print("speaker")
        path_to_csv = './speaker.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Speaker VALUES (%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## TECHNICIAN SHOW ##
        print("tech show")
        path_to_csv = './technician_show.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.TechnicianShow VALUES (%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## TECHNICIAN ##
        print("tech")
        path_to_csv = './technician.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Technician VALUES (%s,%s,%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## THEATER SHOW ##
        print("theatershow")
        path_to_csv = './theater_show.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.TheaterShow VALUES (%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## USER CONTACT ##
        print("user contact")
        path_to_csv = './user_contact.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.UserContact VALUES (%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## VENUE ##
        print("venue")
        path_to_csv = './venue.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.Venue VALUES (%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
        ## VIDEO DEVICE ##
        print("video device")
        path_to_csv = './video_device.csv'
        data = pd.read_csv(path_to_csv, index_col=False, delimiter = ',')

        #loop through the cable data frame
        for i,row in data.iterrows():
            sql = "INSERT INTO theaterDb.VideoDevice VALUES (%s,%s,%s,%s,%s)"
            cursor.execute(sql, tuple(row))
            conn.commit()

 
 
        cursor.close()
        conn.close()
except Error as e:
            print("Error while connecting to MySQL", e)
