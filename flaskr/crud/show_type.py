from ..db.functions import get_db_connection, get_db_connection

def get_all_show_types():
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    cur.execute('SELECT * FROM ShowType;')
    show_types = cur.fetchall()
    response = []
    for st in show_types:
        response.append({
            'name': st[0],
            'type': st[1],
        })
    cur.close()
    conn.close()
    return response

def delete_X_X_X_X(EquipmentID):
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    sql = "DELETE FROM `X_X_X_X` WHERE `X_X_X_X`.`EquipmentID` = '%s';" % EquipmentID

    cur.execute(sql)
    cur.close()

    conn.commit()
    conn.close()
    return


def add_X_X_X_X(c):
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    sql = "INSERT INTO `X_X_X_X`(`DepartmentID`, `Name`, `EquipmentID`, `VenueID`, `X_X_X_X`) VALUES (%s,%s,%s,%s,%s);"
 
    cur.execute(sql, (c['DepartmentID'], c['Name'], c['EquipmentID'], c['VenueID'], c['X_X_X_X']))

    cur.close()
    conn.commit()
    conn.close()
    return

