from ..db.functions import get_db_connection, get_db_connection

def get_all_managers():
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    cur.execute('SELECT * FROM Manager;')
    managers = cur.fetchall()
    response = []
    for m in managers:
        response.append({
            'userID': m[0],
            'lastName': m[1],
            'firstName': m[2],
            'email': m[3],
            'staffNumber': m[4],
            'phoneNumber': m[5],
            'venueID': m[6],
        })
    cur.close()
    conn.close()
    return response

def delete_X_X_X_X(EquipmentID):
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    sql = "DELETE FROM `X_X_X_X` WHERE `X_X_X_X`.`EquipmentID` = '%s';" % EquipmentID

    cur.execute(sql)
    cur.close()

    conn.commit()
    conn.close()
    return


def add_X_X_X_X(c):
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    sql = "INSERT INTO `X_X_X_X`(`DepartmentID`, `Name`, `EquipmentID`, `VenueID`, `X_X_X_X`) VALUES (%s,%s,%s,%s,%s);"
 
    cur.execute(sql, (c['DepartmentID'], c['Name'], c['EquipmentID'], c['VenueID'], c['X_X_X_X']))

    cur.close()
    conn.commit()
    conn.close()
    return

