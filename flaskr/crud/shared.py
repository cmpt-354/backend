from functools import reduce

from sqlalchemy import null
from ..db.functions import get_db_connection

def create_item(mysql, table_name, content):
    rows_txt = ''
    values_txt = ''
    
    for key in content.keys():
        rows_txt += f'{key},'
        values_txt += f"'{content[key]}',"

    sql = f"""\
        INSERT INTO {table_name} ({rows_txt[:-1]})\
        VALUES ({values_txt[:-1]})\
        """

    cursor = mysql.connection.cursor()
    cursor.execute(sql)
    mysql.connection.commit()

    return True

def update_item(mysql, id, uuid_name, table_name, content):
    conn = mysql.connection
    cur = conn.cursor()

    set_txt = ''

    for key in content.keys():
        set_txt += f"{key}='{content[key]}',"

    sql = f"\
        UPDATE {table_name}\
        SET {set_txt[:-1]}\
        WHERE {uuid_name}={id}\
    "
    cur.execute(sql)
    conn.commit()
    return True

def delete_item(mysql, id, uuid_name, table_name):
    conn = mysql.connection
    cur = conn.cursor()
    cur.execute(f'DELETE FROM {table_name} WHERE {uuid_name}={id}')
    conn.commit()
    
    return True 
