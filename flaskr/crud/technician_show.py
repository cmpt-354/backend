from ..db.functions import get_db_connection, get_db_connection

def get_all_technician_shows():
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    cur.execute('SELECT * FROM TechnicianShow;')
    technician_shows = cur.fetchall()
    response = []
    for ts in technician_shows:
        response.append({
            'technicianID': ts[0],
            'showSeason': ts[1],
            'showName': ts[2],
        })
    cur.close()
    conn.close()
    return response

def delete_X_X_X_X(EquipmentID):
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    sql = "DELETE FROM `X_X_X_X` WHERE `X_X_X_X`.`EquipmentID` = '%s';" % EquipmentID

    cur.execute(sql)
    cur.close()

    conn.commit()
    conn.close()
    return


def add_X_X_X_X(c):
    # conn = get_db_connection()
    conn = get_db_connection()

    cur = conn.cursor()
    sql = "INSERT INTO `X_X_X_X`(`DepartmentID`, `Name`, `EquipmentID`, `VenueID`, `X_X_X_X`) VALUES (%s,%s,%s,%s,%s);"
 
    cur.execute(sql, (c['DepartmentID'], c['Name'], c['EquipmentID'], c['VenueID'], c['X_X_X_X']))

    cur.close()
    conn.commit()
    conn.close()
    return

