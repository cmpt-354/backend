import http
import json
from pydoc import allmethods
from unittest import skip
from flask import Flask, jsonify, request
import os
from flask_cors import CORS
from mysql import connector

from .crud import shared

# app = Flask(__name__)

# app.config['CORS_HEADERS'] = 'Content-Type'
from flask import Flask, jsonify, request

from dotenv import load_dotenv
from flask_mysqldb import MySQL

load_dotenv()
 
app = Flask(__name__)
CORS(app)
# Required
app.config["MYSQL_USER"] = os.environ['DB_USERNAME']
app.config["MYSQL_PASSWORD"] = os.environ['DB_PASSWORD']
app.config["MYSQL_DB"] = os.environ['DB_DATABASE']
app.config["MYSQL_HOST"] = os.environ['DB_HOST']
app.config["MYSQL_PORT"] = int(os.environ['DB_PORT'])

# Extra configs, optional:
app.config["MYSQL_CURSORCLASS"] = "DictCursor"

# app.config['MYSQL_DATABASE_USER'] = os.environ['DB_USERNAME']
# app.config['MYSQL_DATABASE_PASSWORD'] = os.environ['DB_PASSWORD']
# app.config['MYSQL_DATABASE_DB'] = os.environ['DB_DATABASE']
# app.config['MYSQL_DATABASE_HOST'] = os.environ['DB_HOST']
# app.config['MYSQL_DATABASE_PORT'] = int(os.environ['DB_PORT'])
mysql = MySQL(app)


###########
## CABLE ##
###########
#ALL
@app.route("/cable/all/")
def cabless():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Cable")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/cable/post/", methods=['POST'])
def add_cable():
    shared.create_item(
        mysql=mysql,
        table_name='Cable',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/cable/<id>/", methods=['PATCH'])
def update_cable(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Cable',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/cable/<id>/", methods=['DELETE'])
def delete_cable(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Cable'
        )
    return ('', 204)

    
############
## CAMERA ##
############
#ALL
@app.route("/camera/all/")
def cameras():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Camera")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/camera/post/", methods=['POST'])
def add_camera():
    shared.create_item(
        mysql=mysql,
        table_name='Camera',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/camera/<id>/", methods=['PATCH'])
def update_camera(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Camera',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/camera/<id>/", methods=['DELETE'])
def delete_camera(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Camera'
        )
    return ('', 204)


##############
## COMPUTER ##
##############

#ALL
@app.route("/computer/all/")
def computers():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Computer")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/computer/post/", methods=['POST'])
def add_computer():
    shared.create_item(
        mysql=mysql,
        table_name='Computer',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/computer/<id>/", methods=['PATCH'])
def update_computer(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Computer',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/computer/<id>/", methods=['DELETE'])
def delete_computer(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Computer'
        )
    return ('', 204)



####################
## DEFAULT DEVICE ##
####################

#ALL
@app.route("/default_device/all/")
def default_devices():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM DefaultDevice")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/default_device/post/", methods=['POST'])
def add_default_device():
    shared.create_item(
        mysql=mysql,
        table_name='DefaultDevice',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/default_device/<id>/", methods=['PATCH'])
def update_default_device(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='DefaultDevice',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/default_device/<id>/", methods=['DELETE'])
def delete_default_device(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='DefaultDevice'
        )
    return ('', 204)



################
## DEPARTMENT ##
################
#ALL
@app.route("/department/all/")
def departments():
    cur = mysql.connection.cursor()
    cur.execute("SELECT Name, ID, count(Technician.UserId) as count FROM Department left outer join Technician on Department.ID=Technician.DepartmentID group by Department.ID")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/department/post/", methods=['POST'])
def add_department():
    shared.create_item(
        mysql=mysql,
        table_name='Department',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/department/<id>/", methods=['PATCH'])
def update_department(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='ID',
        table_name='Department',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/department/<id>/", methods=['DELETE'])
def delete_department(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='ID',
        table_name='Department'
        )
    return ('', 204)

#SEARCH
@app.route("/search/department/<string>/", methods=['GET'])
def department_search(string):
    cur = mysql.connection.cursor()
    cur.execute(f"SELECT * FROM Department WHERE Name LIKE '%{string}%'")
    ret = cur.fetchall()
    return jsonify(ret)

#PROJECTION
@app.route("/fields/department/", methods=['POST'])
def department_projection():
    cur = mysql.connection.cursor()
    txt = ""
    content = request.get_json()
    for key in content.keys():
        if(key == 'count'):
            continue
        txt += f'{key},' if content[key] is True else ""
    
    cur.execute(f"SELECT {txt[:-1]} FROM Department")
    ret = cur.fetchall()
    return jsonify(ret)

#########
## GEL ##
#########

#ALL
@app.route("/gel/all/")
def gels():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Gel")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/gel/post/", methods=['POST'])
def add_gel():
    shared.create_item(
        mysql=mysql,
        table_name='Gel',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/gel/<id>/", methods=['PATCH'])
def update_gel(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Gel',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/gel/<id>/", methods=['DELETE'])
def delete_gel(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Gel'
        )
    return ('', 204)


###########
## LIGHT ##
###########

#ALL
@app.route("/light/all/")
def lights():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Light")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/light/post/", methods=['POST'])
def add_light():
    shared.create_item(
        mysql=mysql,
        table_name='Light',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/light/<id>/", methods=['PATCH'])
def update_light(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Light',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/light/<id>/", methods=['DELETE'])
def delete_light(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Light'
        )
    return ('', 204)



#####################
## LIGHTING DEVICE ##
#####################

#ALL
@app.route("/lighting_device/all/")
def lighting_devices():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM LightingDevice")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/lighting_device/post/", methods=['POST'])
def add_lighting_device():
    shared.create_item(
        mysql=mysql,
        table_name='LightingDevice',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/lighting_device/<id>/", methods=['PATCH'])
def update_lighting_device(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='LightingDevice',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/lighting_device/<id>/", methods=['DELETE'])
def delete_lighting_device(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='LightingDevice'
        )
    return ('', 204)



#############
## MANAGER ##
#############

#ALL
@app.route("/manager/all/")
def managers():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Manager")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/manager/post/", methods=['POST'])
def add_manager():
    shared.create_item(
        mysql=mysql,
        table_name='Manager',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/manager/<id>/", methods=['PATCH'])
def update_manager(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='UserID',
        table_name='Manager',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/manager/<id>/", methods=['DELETE'])
def delete_manager(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='UserID',
        table_name='Manager'
        )
    return ('', 204)


#########
## MIC ##
#########


#ALL
@app.route("/mic/all/")
def mics():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Mic")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/mic/post/", methods=['POST'])
def add_mic():
    shared.create_item(
        mysql=mysql,
        table_name='Mic',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/mic/<id>/", methods=['PATCH'])
def update_mic(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Mic',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/mic/<id>/", methods=['DELETE'])
def delete_mic(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Mic'
        )
    return ('', 204)

#############
## MONITOR ##
#############

#ALL
@app.route("/monitor/all/")
def monitors():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Monitor")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/monitor/post/", methods=['POST'])
def add_monitor():
    shared.create_item(
        mysql=mysql,
        table_name='Monitor',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/monitor/<id>/", methods=['PATCH'])
def update_monitor(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Monitor',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/monitor/<id>/", methods=['DELETE'])
def delete_monitor(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Monitor'
        )
    return ('', 204)


###############
## PROJECTOR ##
###############

#ALL
@app.route("/projector/all/")
def projectors():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Projector")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/projector/post/", methods=['POST'])
def add_projector():
    shared.create_item(
        mysql=mysql,
        table_name='Projector',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/projector/<id>/", methods=['PATCH'])
def update_projector(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Projector',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/projector/<id>/", methods=['DELETE'])
def delete_projector(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Projector'
        )
    return ('', 204)


####################
## SHOW EQUIPMENT ##
####################

#ALL
@app.route("/show_equipment/all/")
def show_equipments():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM ShowEquipment")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/show_equipment/post/", methods=['POST'])
def add_show_equipment():
    shared.create_item(
        mysql=mysql,
        table_name='ShowEquipment',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/show_equipment/<id>/", methods=['PATCH'])
def update_show_equipment(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='ShowEquipment',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/show_equipment/<id>/", methods=['DELETE'])
def delete_show_equipment(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='ShowEquipment'
        )
    return ('', 204)


###############
## SHOW TYPE ##
###############

#ALL
@app.route("/show_type/all/")
def show_types():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM ShowType")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/show_type/post/", methods=['POST'])
def add_show_type():
    shared.create_item(
        mysql=mysql,
        table_name='ShowType',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/show_type/<id>/", methods=['PATCH'])
def update_show_type(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='Name',
        table_name='ShowType',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/show_type/<id>/", methods=['DELETE'])
def delete_show_type(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='Name',
        table_name='ShowType'
        )
    return ('', 204)


##################
## SOUND DEVICE ##
##################

#ALL
@app.route("/sound_device/all/")
def sound_devices():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM SoundDevice")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/sound_device/post/", methods=['POST'])
def add_sound_device():
    shared.create_item(
        mysql=mysql,
        table_name='SoundDevice',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/sound_device/<id>/", methods=['PATCH'])
def update_sound_device(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='SoundDevice',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/sound_device/<id>/", methods=['DELETE'])
def delete_sound_device(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='SoundDevice'
        )
    return ('', 204)



#############
## SPEAKER ##
#############

#ALL
@app.route("/speaker/all/")
def speakers():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Speaker")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/speaker/post/", methods=['POST'])
def add_speaker():
    shared.create_item(
        mysql=mysql,
        table_name='Speaker',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/speaker/<id>/", methods=['PATCH'])
def update_speaker(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Speaker',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/speaker/<id>/", methods=['DELETE'])
def delete_speaker(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='Speaker'
        )
    return ('', 204)


#####################
## TECHNICIAN SHOW ##
#####################

#ALL
@app.route("/technician_show/all/")
def technician_shows():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM TechnicianShow")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/technician_show/post/", methods=['POST'])
def add_technician_show():
    shared.create_item(
        mysql=mysql,
        table_name='TechnicianShow',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/technician_show/<id>/", methods=['PATCH'])
def update_technician_show(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='TechnicianID',
        table_name='TechnicianShow',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/technician_show/<id>/", methods=['DELETE'])
def delete_technician_show(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='TechnicianID',
        table_name='TechnicianShow'
        )
    return ('', 204)


################
## TECHNICIAN ##
################

#ALL
@app.route("/technician/all/")
def technicians():
    cur = mysql.connection.cursor()
    cur.execute("\
        SELECT * FROM Technician\
        INNER JOIN User ON Technician.UserId=User.ID\
        INNER JOIN Department ON Technician.DepartmentId=Department.ID\
    ")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/technician/post/", methods=['POST'])
def add_technician():
    cur = mysql.connection.cursor()
    content = request.get_json()
    cur.execute(f"\
        INSERT INTO User(FirstName, LastName, Email, StaffNumber, Active) \
        VALUES('{content['FirstName']}', '{content['LastName']}', '{content['Email']}', {content['StaffNumber']}, {True})\
    ")
    cur.execute("SELECT LAST_INSERT_ID();")
    user = cur.fetchone()
    cur.execute(f"\
        INSERT INTO Technician(UserID, DepartmentID) \
        VALUES ({user['LAST_INSERT_ID()']}, {content['DepartmentID']})\
    ")
    mysql.connection.commit()
    response = cur.execute("SELECT * FROM Technician WHERE UserId = LAST_INSERT_ID()")
    return jsonify(response)

#UPDATE
@app.route("/technician/<id>/", methods=['PATCH'])
def update_technician(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='UserID',
        table_name='Technician',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/technician/<id>/", methods=['DELETE'])
def delete_technician(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='UserID',
        table_name='Technician'
        )
    return ('', 204)


##################
## THEATER SHOW ##
##################

#ALL
@app.route("/theater_show/all/")
def theater_shows():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM TheaterShow")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/theater_show/post/", methods=['POST'])
def add_theater_show():
    shared.create_item(
        mysql=mysql,
        table_name='TheaterShow',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/theater_show/<id>/", methods=['PATCH'])
def update_theater_show(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='Name',
        table_name='TheaterShow',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/theater_show/<id>/", methods=['DELETE'])
def delete_theater_show(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='Name',
        table_name='TheaterShow'
        )
    return ('', 204)


##################
## USER CONTACT ##
##################

#ALL
@app.route("/user_contact/all/")
def user_contacts():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM UserContact")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/user_contact/post/", methods=['POST'])
def add_user_contact():
    shared.create_item(
        mysql=mysql,
        table_name='UserContact',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/user_contact/<id>/", methods=['PATCH'])
def update_user_contact(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='UserID',
        table_name='UserContact',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/user_contact/<id>/", methods=['DELETE'])
def delete_user_contact(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='UserID',
        table_name='UserContact'
        )
    return ('', 204)


###########
## VENUE ##
###########

#ALL
@app.route("/venue/all/")
def venues():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM Venue")
    rv = cur.fetchall()
    for venue in rv:
        cur.execute(f"SELECT count(*) as count FROM Equipment WHERE VenueID={venue['ID']}")
        count = cur.fetchone()
        venue['count'] = count['count']
    return jsonify(rv)

#ADD
@app.route("/venue/post/", methods=['POST'])
def add_venue():
    shared.create_item(
        mysql=mysql,
        table_name='Venue',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/venue/<id>/", methods=['PATCH'])
def update_venue(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='ID',
        table_name='Venue',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/venue/<id>/", methods=['DELETE'])
def delete_venue(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='ID',
        table_name='Venue'
        )
    return ('', 204)



##################
## VIDEO DEVICE ##
##################

#ALL
@app.route("/video_device/all/")
def video_devices():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM VideoDevice")
    rv = cur.fetchall()
    return jsonify(rv)

#ADD
@app.route("/video_device/post/", methods=['POST'])
def add_video_device():
    shared.create_item(
        mysql=mysql,
        table_name='VideoDevice',
        content=request.get_json()
    )
    return ('', 204)

#UPDATE
@app.route("/video_device/<id>/", methods=['PATCH'])
def update_video_device(id):
    shared.update_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='VideoDevice',
        content=request.get_json()
    )
    return ('', 204)

#DELETE
@app.route("/video_device/<id>/", methods=['DELETE'])
def delete_video_device(id):
    shared.delete_item(
        mysql=mysql,
        id=id,
        uuid_name='EquipmentID',
        table_name='VideoDevice'
        )
    return ('', 204)






@app.route("/")
def index():
    return "Catchphrase!!"
  
if __name__ == "__main__":
    app.run(debug=True)



# ###############
# #  TEMPLATE  ##
# ###############

# #ALL
# @app.route("/X_X_X_X/all/")
# def X_X_X_Xs():
#     cur = mysql.connection.cursor()
#     cur.execute("SELECT * FROM X_X_X_X")
#     rv = cur.fetchall()
#     return jsonify(rv)

# #ADD
# @app.route("/X_X_X_X/post/", methods=['POST'])
# def add_X_X_X_X():
#     shared.create_item(
#         mysql=mysql,
#         table_name='X_X_X_X',
#         content=request.get_json()
#     )
#     return ('', 204)

# #UPDATE
# @app.route("/X_X_X_X/<id>/", methods=['PATCH'])
# def update_X_X_X_X(id):
#     shared.update_item(
#         mysql=mysql,
#         id=id,
#         uuid_name='X_X_X_X',
#         table_name='X_X_X_X',
#         content=request.get_json()
#     )
#     return ('', 204)

# #DELETE
# @app.route("/X_X_X_X/<id>/", methods=['DELETE'])
# def delete_X_X_X_X(id):
#     shared.delete_item(
#         mysql=mysql,
#         id=id,
#         uuid_name='X_X_X_X',
#         table_name='X_X_X_X'
#         )
#     return ('', 204)
